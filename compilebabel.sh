#!/bin/bash

pybabel -v extract -F babel.config -o ./locale/messages.pot ./templates/
pybabel compile -f -d locale/

#Delete dictionarys, only execute first time
#pybabel init -l en_US -d ./locale -i ./locale/messages.pot
#pybabel init -l es_ES -d ./locale -i ./locale/messages.pot

pybabel update -l en_US -d ./locale -i ./locale/messages.pot
pybabel update -l es_ES -d ./locale -i ./locale/messages.pot

#rm output/* -Rf
sleep 1
python script.py
#chown www-data.www-data output/ -Rf


#Copy static
cp -r static/. output/static